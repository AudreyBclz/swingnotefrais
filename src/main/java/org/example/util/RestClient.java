package org.example.util;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


@Service
public class RestClient<T,V> {
    private String server = "http://localhost:8083";
    private RestTemplate template;
    private HttpHeaders headers;
    private HttpStatusCode status;

    public RestClient(){
        template = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        headers = new HttpHeaders();
        headers.add("Accept","*/*");
        headers.add("content-type","application/json");

        try{
            BufferedReader br = new BufferedReader(new FileReader("token.txt"));
            String line;
            String token="";
            while((line = br.readLine())!=null){
                token=line;
            }
            if(token.length()>0){
                headers.add("Authorization",token);
            }
        }catch (IOException ignored){

        }
    }

    public T get(String uri,Class<T>type){
        HttpEntity<String> requestEntity = new HttpEntity<>("",headers);
        ResponseEntity<T> responseEntity = template.exchange(server+uri,HttpMethod.GET,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
    public T patch(String uri,V data,Class<T> type){
        HttpEntity<V> requestEntity = new HttpEntity<>(data,headers);
        ResponseEntity<T> responseEntity = template.exchange(server+uri,HttpMethod.PATCH,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }

    public T post(String uri,V data,Class<T> type){
        HttpEntity<V>requestEntity = new HttpEntity<>(data,headers);
        ResponseEntity<T> responseEntity = template.exchange(server+uri,HttpMethod.POST,requestEntity,type);
        status =responseEntity.getStatusCode();
        return responseEntity.getBody();
    }

}
