package org.example;

import com.example.librairienotefrais.dto.ResponseExpenseReportDto;
import com.example.librairienotefrais.enums.Status;
import lombok.Data;
import org.example.adapter.ButtonRenderer;
import org.example.adapter.ExpenseReportModel;
import org.example.adapter.MouseEvent;
import org.example.service.ExpenseReportService;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;


@Data
public class ListExpenseReport {
    private JPanel mainPanel;
    private JTable table;
    private JLabel message;
    private JFrame _frame;
    private ExpenseReportService expenseReportService;
    private ResponseExpenseReportDto[] reportDtos;
    private GridBagConstraints constraints;

    public ListExpenseReport(JFrame frame){
        _frame=frame;
        mainPanel.setLayout(new GridBagLayout());
        constraints = new GridBagConstraints();
        setConstraints(constraints);
        expenseReportService = new ExpenseReportService();
        table = new JTable();
        reportDtos = expenseReportService.findAll();
        ArrayList<ResponseExpenseReportDto> dtoList = new ArrayList<>();
        for(ResponseExpenseReportDto dto: reportDtos){
            if(dto.getStatus()== Status.INPROGRESS || dto.getStatus()==Status.VALIDATED){
                dtoList.add(dto);
            }
        }
        ExpenseReportModel model = new ExpenseReportModel(dtoList);
        table.setModel(model);
        table.getColumn("Payer").setCellRenderer(new ButtonRenderer());
        table.getColumn("Valider").setCellRenderer(new ButtonRenderer());
        table.getColumn("Refuser").setCellRenderer(new ButtonRenderer());
        constraints=new GridBagConstraints();
        constraints.gridwidth=1;
        constraints.fill= GridBagConstraints.HORIZONTAL;
        MouseEvent mouseEvent = new MouseEvent(table,_frame);
        JScrollPane scrollPane = new JScrollPane(table);
        mainPanel.add(scrollPane,constraints);
    }
}
