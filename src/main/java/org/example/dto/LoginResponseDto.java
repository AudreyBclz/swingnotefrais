package org.example.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;



@Data
@ToString
@NoArgsConstructor
public class LoginResponseDto {
    private String token;
    private String username;
    private int id;
}
