package org.example.service;

import com.example.librairienotefrais.dto.RequestExpenseReportDto;
import com.example.librairienotefrais.dto.ResponseExpenseReportDto;
import org.example.dto.LoginRequestDto;
import org.example.dto.LoginResponseDto;
import org.example.util.RestClient;


public class ExpenseReportService {
    private RestClient _restClient;

    public ExpenseReportService(){ _restClient = new RestClient<>();}

    public LoginResponseDto login(LoginRequestDto loginRequest){
        return (LoginResponseDto) _restClient.post("/login",loginRequest, LoginResponseDto.class);
    }
    public ResponseExpenseReportDto[] findAll(){
        return (ResponseExpenseReportDto[]) _restClient.get("/expense-report",ResponseExpenseReportDto[].class);
    }
    public ResponseExpenseReportDto updateStatus(RequestExpenseReportDto dto,int id){
        return (ResponseExpenseReportDto) _restClient.patch("/expense-report/"+id,dto, ResponseExpenseReportDto.class);
    }
}
