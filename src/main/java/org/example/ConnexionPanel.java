package org.example;

import lombok.Data;
import org.example.dto.LoginRequestDto;
import org.example.dto.LoginResponseDto;
import org.example.service.ExpenseReportService;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

@Data
public class ConnexionPanel {
    private JPanel mainPanel;
    private JLabel TitreLabel;
    private JTextField usernameField;
    private JTextField passwordField;
    private JButton connexionButton;
    private JLabel message;
    private JFrame _frame;


    private ExpenseReportService _expenseReportService;
    public ConnexionPanel(JFrame frame){
        _frame=frame;
        _expenseReportService=new ExpenseReportService();
        connexionButton.addActionListener((e)->{
            try{
                if(usernameField.getText()!=null && passwordField.getText()!=null) {
                    LoginRequestDto loginRequest = new LoginRequestDto();
                    System.out.println(usernameField.getText());
                     loginRequest =LoginRequestDto.builder().username(usernameField.getText()).password(passwordField.getText()).build();
                     try{
                         LoginResponseDto response =_expenseReportService.login(loginRequest);
                         BufferedWriter writer = new BufferedWriter(new FileWriter("token.txt"));
                         writer.write(response.getToken());
                         writer.close();
                         _frame.setContentPane(new ListExpenseReport(frame).getMainPanel());
                         _frame.setSize(700,700);
                         _frame.revalidate();


                     }catch (Exception exception){
                         message.setText("<html><font color='red'>Mot de passe ou identifiant incorrect</font></html>");
                         System.out.println(exception.getMessage());
                     }

                }
            }catch (Exception ignored){

            }
        });
    }
}
