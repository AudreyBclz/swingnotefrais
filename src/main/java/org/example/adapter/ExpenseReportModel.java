package org.example.adapter;

import com.example.librairienotefrais.dto.RequestExpenseReportDto;
import com.example.librairienotefrais.dto.ResponseExpenseReportDto;
import com.example.librairienotefrais.enums.Status;
import org.example.service.ExpenseReportService;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.List;

public class ExpenseReportModel extends AbstractTableModel {
    private List<ResponseExpenseReportDto> expenseReports;

    private ExpenseReportService expenseReportService;
    String header[] = new String[]{"Id","Titre","Date","Montant","Statut","Payer","Valider","Refuser"};

    public ExpenseReportModel(List<ResponseExpenseReportDto>er){
        expenseReports = er;
        expenseReportService = new ExpenseReportService();
    }

    @Override
    public int getRowCount() {
        return expenseReports.size() ;
    }

    @Override
    public int getColumnCount() {
        return header.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ResponseExpenseReportDto er = expenseReports.get(rowIndex);
        switch (columnIndex){
            case 0:
                return er.getId();
            case 1:
                return er.getTitle();
            case 2:
                return er.getDate();
            case 3:
                return er.getAmount()+" $";
                case 4:
                    return er.getStatus();
            case 5:
                if(er.getStatus()== Status.VALIDATED){
                    JButton payerButton = new JButton("payer");
                    return payerButton;
                }
                else return null;
            case 6:
                if(er.getStatus()==Status.INPROGRESS)
                    return new JButton("Valider");
                else return null;
            case 7:
                if(er.getStatus()==Status.INPROGRESS)
                    return new JButton("Refuser");
                else return null;
            default:
                return null;

        }
    }
    public String getColumnName(int col){return header[col];}
}
