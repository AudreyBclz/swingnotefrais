package org.example.adapter;

import com.example.librairienotefrais.dto.RequestExpenseReportDto;
import com.example.librairienotefrais.dto.ResponseExpenseReportDto;
import com.example.librairienotefrais.enums.Status;
import org.example.ListExpenseReport;
import org.example.service.ExpenseReportService;

import javax.swing.*;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class MouseEvent implements MouseListener {

    private ExpenseReportService service;
    private JTable table;
    private JFrame frame;
    public MouseEvent(JTable table,JFrame frame){
        this.table=table;
        table.addMouseListener(this);
        this.service=new ExpenseReportService();
        this.frame = frame;
    }

    @Override
    public void mouseClicked(java.awt.event.MouseEvent e) {
        table= (JTable) e.getSource();
        if(table.isColumnSelected(5)){
            int row = table.getSelectedRow();
           if(table.getValueAt(row,4)== Status.VALIDATED){
               RequestExpenseReportDto dto = new RequestExpenseReportDto();
               dto.setStatus(Status.PAID);
               int id = (int) table.getValueAt(row,0);
               service.updateStatus(dto,id);

           }
        }
        else if(table.isColumnSelected(6)){
            int row = table.getSelectedRow();
            if(table.getValueAt(row,4)== Status.INPROGRESS){
                RequestExpenseReportDto dto = new RequestExpenseReportDto();
                dto.setStatus(Status.VALIDATED);
                int id = (int) table.getValueAt(row,0);
                service.updateStatus(dto,id);
            }
        }
        else if(table.isColumnSelected(7)){
            int row = table.getSelectedRow();
            if(table.getValueAt(row,4)== Status.INPROGRESS){
                RequestExpenseReportDto dto = new RequestExpenseReportDto();
                dto.setStatus(Status.REFUSED);
                int id = (int) table.getValueAt(row,0);
                service.updateStatus(dto,id);
            }
        }
        ResponseExpenseReportDto[] reportDtos = service.findAll();
        ArrayList<ResponseExpenseReportDto> dtoList = new ArrayList<>();
        for(ResponseExpenseReportDto dto: reportDtos){
            if(dto.getStatus()== Status.INPROGRESS || dto.getStatus()==Status.VALIDATED){
                dtoList.add(dto);
            }
        }
        ExpenseReportModel model = new ExpenseReportModel(dtoList);
        frame.setContentPane(new ListExpenseReport(frame).getMainPanel());
        frame.setSize(600,600);
        frame.revalidate();

    }

    @Override
    public void mousePressed(java.awt.event.MouseEvent e) {

    }

    @Override
    public void mouseReleased(java.awt.event.MouseEvent e) {

    }

    @Override
    public void mouseEntered(java.awt.event.MouseEvent e) {

    }

    @Override
    public void mouseExited(java.awt.event.MouseEvent e) {

    }
}
