package org.example.adapter;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class ButtonRenderer extends JButton implements TableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if(value ==null){
            return null;
        }
        return (Component) value;
        }
    public ButtonRenderer(){
        setOpaque(true);
    }
}

