package org.example;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        JFrame expenseReportFrame = new JFrame("AAC Expense-Report");
        expenseReportFrame.setSize(new Dimension(500,300));
        expenseReportFrame.setContentPane(new ConnexionPanel(expenseReportFrame).getMainPanel());
        expenseReportFrame.setVisible(true);
    }
}